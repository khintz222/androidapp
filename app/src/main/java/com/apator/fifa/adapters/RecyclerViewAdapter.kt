package com.apator.fifa.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.apator.fifa.R
import com.apator.fifa.databases.entitis.PlayerEntity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.player_item.view.*

class RecyclerViewAdapter(private val myData: ArrayList<PlayerEntity>) :
    RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.player_item, parent, false)

        return MyViewHolder(view)

    }

    override fun getItemCount() = myData.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.item.playerNameTxIntem.text = myData[position].name
        holder.item.playerAgeTxIntem.text = myData[position].age.toString()
        holder.item.playerClubTxIntem.text = myData[position].club
        Picasso.get()
            .load(myData[position].photo)
            .resize(200, 200)
            .into(holder.item.playerImgItem)
        Picasso.get()
            .load(myData[position].clubLogo)
            .resize(150, 150)
            .into(holder.item.playerClubImgItem)
        Picasso.get()
            .load(myData[position].flag)
            .resize(150, 150)
            .into(holder.item.playerFlagImgItem)



        holder.item.cardView.setOnClickListener { view ->
            val bundle = Bundle()
            bundle.putInt("PLAYER_ID", myData[position].id!!)
            view.findNavController().navigate(R.id.action_playerListFragment_to_singlePlayerFragment,bundle)

        }

    }


    class MyViewHolder(val item: View) : RecyclerView.ViewHolder(item)


}
