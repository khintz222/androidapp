package com.apator.fifa

import android.app.Application
import com.apator.fifa.databases.viewmodels.PlayerViewModel
import com.apator.fifa.databases.viewmodels.UserViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    module {
                        viewModel { UserViewModel(androidApplication()) }
                    },
                            module {
                        viewModel { PlayerViewModel(androidApplication()) }
                    }

                )
            )

        }
    }
}