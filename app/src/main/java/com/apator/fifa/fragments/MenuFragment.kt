package com.apator.fifa.fragments


import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.findNavController

import com.apator.fifa.R
import com.apator.fifa.databases.entitis.PlayerEntity
import com.apator.fifa.databases.entitis.UserEntity
import com.apator.fifa.databases.viewmodels.PlayerViewModel
import com.apator.fifa.databases.viewmodels.UserViewModel

import com.apator.fifa.helpers.filereader.FifaFileReader
import com.apator.fifa.helpers.mappers.PlayerDbMapper
import com.apator.fifa.helpers.mappers.PlayerMaper
import com.apator.fifa.model.Player
import kotlinx.android.synthetic.main.fragment_menu.*
import org.koin.android.viewmodel.ext.android.viewModel

class MenuFragment : Fragment() {
    val userViewModel: UserViewModel by viewModel()
    val playerViewModel:PlayerViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val userId = arguments!!.getInt("USER_ID")

        showUserName(userId)
        exitBtnMenu.setOnClickListener{ System.exit(1)}




        loadFileBtnMenu.setOnClickListener {
            performFileSearch()
        }

        showPlayersBtnMenu.setOnClickListener {
            view.findNavController().navigate(R.id.action_menuFragment_to_playerListFragment)
        }
    }

    private fun showUserName(userId: Int) {
        userViewModel.getUserById(userId).observe(this, Observer {
            val userName = it.userName
            userNameTxMenu.setText("Logged as $userName")
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            var lines: ArrayList<String>
            data!!.let { intent ->
                lines = FifaFileReader.read(data.data!!, context!!.contentResolver)
            }
            val players = arrayListOf<Player>()
            val playersDb = arrayListOf<PlayerEntity>()


            lines.forEach {
                players.add(PlayerMaper.map(it))
            }
            players.forEach { playersDb.add(PlayerDbMapper.map(it)) }

            playersDb.forEach {
                playerViewModel.insertPlayer(it)}

        }
    }


    private fun performFileSearch() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "text/*"
        startActivityForResult(intent, READ_REQUEST_CODE)
    }


    companion object {
        const val READ_REQUEST_CODE = 123
    }


}
