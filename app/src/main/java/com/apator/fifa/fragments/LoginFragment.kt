package com.apator.fifa.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.apator.fifa.R
import com.apator.fifa.databases.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.android.viewmodel.ext.android.viewModel


class LoginFragment : Fragment() {
    val userViewModel: UserViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        registerBtnLogin.setOnClickListener {
            view.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
        loginBtnLogin.setOnClickListener {

            val userName = nameETLogin.text.toString()
            val userPassword = passwordETLogin.text.toString()
            if(userName.isNotEmpty() && userPassword.isNotEmpty()){
            userViewModel.getUserByLoginAndPassword(userName,userPassword).observe(this, Observer {
                if(it!=null)
                {
                    Toast.makeText(context,"You are logged in",Toast.LENGTH_SHORT).show()
                    val bundle = Bundle()
                    bundle.putInt("USER_ID",it.id!!)
                    view.findNavController().navigate(R.id.action_loginFragment_to_menuFragment,bundle)
                }else
                {
                    Toast.makeText(context,"Check credentials",Toast.LENGTH_SHORT).show()
                }
            })}else{
                Toast.makeText(context,"Fill all fields",Toast.LENGTH_SHORT).show()
            }

        }
    }
}
