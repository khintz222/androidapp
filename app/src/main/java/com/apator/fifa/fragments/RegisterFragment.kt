package com.apator.fifa.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.findNavController

import com.apator.fifa.R
import com.apator.fifa.databases.entitis.UserEntity
import com.apator.fifa.databases.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.fragment_register.*
import org.koin.android.viewmodel.ext.android.viewModel

class RegisterFragment : Fragment() {
    val userViewModel: UserViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        backBtnRegister.setOnClickListener {
            view.findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
        registerBttnRegister.setOnClickListener {

            val nickName = nickNameETRegister.text.toString()
            val email = emailETRegister.text.toString()
            val password = passwordETRegister.text.toString()

            if (nickName.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty()) {



                userViewModel.getUserByLogin(nickName).observe(this, Observer {user->
                    if (user != null) {
                        Toast.makeText(context, "login is use", Toast.LENGTH_SHORT).show()
                    }
                    userViewModel.getUserByEmail(email).observe(this, Observer {
                        if (it != null) {
                            Toast.makeText(context, "email is use", Toast.LENGTH_SHORT).show()
                        }else if(it == null && user == null )
                        {
                            Toast.makeText(context, "Account created.", Toast.LENGTH_SHORT).show()
                            userViewModel.insertUser(UserEntity(null,nickName,email,password))
                            view.findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
                        }
                    })

                })



            } else {
                Toast.makeText(context, "Fill all fields", Toast.LENGTH_SHORT).show()
            }
        }

    }

   }
