package com.apator.fifa.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer

import com.apator.fifa.R
import com.apator.fifa.databases.viewmodels.PlayerViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_single_player.*
import org.koin.android.viewmodel.ext.android.viewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.*
 */
class SinglePlayerFragment : Fragment() {
private val playerViewModel:PlayerViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_single_player, container, false)
        val playerId = arguments!!.getInt("PLAYER_ID")
        playerViewModel.getPlayerById(playerId).observe(this, Observer {
            Picasso.get()
                .load(it.photo)
                .resize(400,400)
                .into(singlePlayerImg)
            singlePlayerName.text = it.name
            singlePlayerAge.text = it.age.toString()
            singlePlayerClub.text = it.club
        })
        return view
    }


}
