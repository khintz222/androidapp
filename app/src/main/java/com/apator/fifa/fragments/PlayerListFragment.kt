package com.apator.fifa.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apator.fifa.R
import com.apator.fifa.adapters.RecyclerViewAdapter
import com.apator.fifa.databases.entitis.PlayerEntity
import com.apator.fifa.databases.viewmodels.PlayerViewModel
import kotlinx.android.synthetic.main.fragment_player_list.view.*
import org.koin.android.viewmodel.ext.android.viewModel


class PlayerListFragment : Fragment() {

    private val playerViewModel: PlayerViewModel by viewModel()
    private val players = arrayListOf<PlayerEntity>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_player_list, container, false)
        val playerAdapter = RecyclerViewAdapter(players)
        val playersRecyclerView = view.recyclerViewPlayerList
        playersRecyclerView.adapter = playerAdapter
        playersRecyclerView.layoutManager = LinearLayoutManager(view.context)

        playerViewModel.getAllPlayers().observe(this, Observer<List<PlayerEntity>> {
            players.addAll(it)
            playerAdapter.notifyDataSetChanged()
        })
        return view
    }


}
