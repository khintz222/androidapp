package com.apator.fifa.helpers.mappers

import com.apator.fifa.model.Player

object PlayerMaper {
    fun map(line: String): Player {

        val data = line.split(';')
        return Player(
            num = data[0].toInt(),
            id = data[1].toInt(),
            name = data[2],
            age = data[3].toInt(),
            photo = data[4],
            nationality = data[5],
            flag = data[6],
            overall = data[7].toInt(),
            potential = data[8].toInt(),
            club = data[9],
            clubLogo = data[10],
            value = data[11],
            wage = data[12]
        )


    }
}
