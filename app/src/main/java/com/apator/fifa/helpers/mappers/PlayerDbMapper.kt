package com.apator.fifa.helpers.mappers

import com.apator.fifa.databases.entitis.PlayerEntity
import com.apator.fifa.model.Player

object PlayerDbMapper {
    fun map(player: Player) =
        PlayerEntity(
            player.num,
            player.name,
            player.age,
            player.photo,
            player.nationality,
            player.flag,
            player.overall,
            player.potential,
            player.club,
            player.clubLogo,
            player.value,
            player.wage
        )

}