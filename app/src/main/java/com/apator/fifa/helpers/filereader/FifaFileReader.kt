package com.apator.fifa.helpers.filereader

import android.content.ContentResolver
import android.net.Uri
import java.io.BufferedReader
import java.io.InputStreamReader

object FifaFileReader {
    fun read(uri: Uri, contentResolver: ContentResolver):ArrayList<String>
    {
        val file = contentResolver.openInputStream(uri)
        val stream = InputStreamReader(file!!)
        val lines = ArrayList(BufferedReader(stream).readLines())
        lines.removeAt(0)
        return lines
    }
}
