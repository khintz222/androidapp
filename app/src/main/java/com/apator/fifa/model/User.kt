package com.apator.fifa.model


data class User(
    val id: Int?,
    val userName: String,
    val email: String,
    val password: String

)
