package com.apator.fifa.databases.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.apator.fifa.databases.entitis.PlayerEntity
import com.apator.fifa.databases.repositories.PlayerRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PlayerViewModel(application: Application) : AndroidViewModel(application) {
    private val playerRepository = PlayerRepository(application)

    fun getAllPlayers() = playerRepository.players
    fun getPlayerById(id: Int) = playerRepository.playerDao.getPlayerById(id)

    fun insertPlayer(player: PlayerEntity) = viewModelScope.launch(Dispatchers.IO) {
        playerRepository.insertPlayer(player)
    }

    fun insertAllPlayers(players: List<PlayerEntity>) = viewModelScope.launch(Dispatchers.IO) {
        playerRepository.insertAllPlayers(players)
    }
}