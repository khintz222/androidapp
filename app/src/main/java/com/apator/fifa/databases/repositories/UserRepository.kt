package com.apator.fifa.databases.repositories


import android.content.Context
import androidx.annotation.WorkerThread
import com.apator.fifa.databases.UserDatabase
import com.apator.fifa.databases.entitis.UserEntity

class UserRepository(context: Context) {
    private val db = UserDatabase.getDatabase(context)
    val userDao = db.userDao()


    @WorkerThread
    suspend fun insertUser(userEntity: UserEntity) {
        userDao.insertUser(userEntity)
    }

}
