package com.apator.fifa.databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.apator.fifa.databases.dao.PlayerDao
import com.apator.fifa.databases.entitis.PlayerEntity


@Database(entities = [PlayerEntity::class], version = 1)
abstract class PlayersDatabase : RoomDatabase() {
    abstract fun playerDao(): PlayerDao

    companion object {
        var INSTANCE: PlayersDatabase? = null

        fun getDatabase(context: Context): PlayersDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = buildDatabase(context)
                INSTANCE = instance
                return instance
            }
        }


        private fun buildDatabase(context: Context): PlayersDatabase {
            return Room.databaseBuilder(context, PlayersDatabase::class.java, "players").build()
        }

    }
}