package com.apator.fifa.databases.entitis

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "players")
data class PlayerEntity(
    @PrimaryKey() val id: Int?,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "age") val age: Int?,
    @ColumnInfo(name = "photo") val photo: String?,
    @ColumnInfo(name = "nationality") val nationality: String?,
    @ColumnInfo(name = "flag") val flag: String?,
    @ColumnInfo(name = "overall") val overall: Int?,
    @ColumnInfo(name = "potential") val potential: Int?,
    @ColumnInfo(name = "club") val club: String?,
    @ColumnInfo(name = "clubLogo") val clubLogo: String?,
    @ColumnInfo(name = "value") val value: String?,
    @ColumnInfo(name = "wage") val wage: String?
)