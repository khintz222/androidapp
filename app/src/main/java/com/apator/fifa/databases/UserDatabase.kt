package com.apator.fifa.databases


import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.apator.fifa.databases.dao.UserDao
import com.apator.fifa.databases.entitis.UserEntity

@Database(entities = [UserEntity::class], version = 1)
abstract class UserDatabase : RoomDatabase() {
abstract fun userDao() : UserDao

    companion object{
        var INSTANCE: UserDatabase? = null

        fun getDatabase(context: Context): UserDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(context, UserDatabase::class.java, "user").build()
                INSTANCE = instance
                return instance
            }
        }

    }
}