package com.apator.fifa.databases.repositories

import android.content.Context
import androidx.annotation.WorkerThread
import com.apator.fifa.databases.PlayersDatabase
import com.apator.fifa.databases.entitis.PlayerEntity

class PlayerRepository(context: Context) {
    val db = PlayersDatabase.getDatabase(context)
    val playerDao = db.playerDao()
    val players = playerDao.getAllPlayers()

    @WorkerThread
    suspend fun insertAllPlayers(players: List<PlayerEntity>) {
        playerDao.insertAll(*players.toTypedArray())
    }

    @WorkerThread
    suspend fun insertPlayer(player: PlayerEntity) {
        playerDao.insertPlayer(player)
    }
}