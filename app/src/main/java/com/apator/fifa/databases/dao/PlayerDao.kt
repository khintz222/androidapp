package com.apator.fifa.databases.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.apator.fifa.databases.entitis.PlayerEntity

@Dao
interface PlayerDao {

    @Query("SELECT * FROM players WHERE id=:id")
    fun getPlayerById(id: Int): LiveData<PlayerEntity>

    @Query("SELECT * FROM players")
    fun getAllPlayers(): LiveData<List<PlayerEntity>>

    @Insert
    fun insertPlayer(player: PlayerEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg players: PlayerEntity)
}