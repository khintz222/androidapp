package com.apator.fifa.databases.viewmodels

import android.app.Application
import android.provider.ContactsContract
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.apator.fifa.databases.entitis.UserEntity
import com.apator.fifa.databases.repositories.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class UserViewModel(applcation: Application) : AndroidViewModel(applcation) {
    private val userRepository = UserRepository(applcation)


    fun insertUser(userEntity: UserEntity) = viewModelScope.launch(Dispatchers.IO) {
        userRepository.insertUser(userEntity)
    }

//
//    fun getUserByLogin(login: String) = userRepository.userDao.getUserByLogin(login)
//
//
//    fun getUserByEmail(email: String) = userRepository.userDao.getUserByEmail(email)
//

    fun getUserByLoginAndPassword(login: String, password: String): LiveData<UserEntity?> =
        userRepository.userDao.getUserByLoginAndPassword(login, password)

    fun getUserById(id: Int): LiveData<UserEntity> =
        userRepository.userDao.getUserById(id)

    fun getUserByLogin(login: String) = userRepository.userDao.getUserByLogin(login)
    fun getUserByEmail(email: String) = userRepository.userDao.getUserByEmail(email)
}