package com.apator.fifa.databases.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.apator.fifa.databases.entitis.UserEntity
import com.apator.fifa.model.User


@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(userEntity: UserEntity)

//    @Query("SELECT * FROM user WHERE userName == :login")
//    fun getUserByLogin(login: String): ArrayList<UserEntity>
//
//    @Query("SELECT * FROM user WHERE email == :email")
//    fun getUserByEmail(email: String):ArrayList<UserEntity>

    @Query("SELECT * FROM user WHERE userName=:login AND password == :password LIMIT 1")
    fun getUserByLoginAndPassword(login: String, password: String): LiveData<UserEntity?>

    @Query("SELECT * FROM user WHERE id==:id")
    fun getUserById(id: Int): LiveData<UserEntity>

    @Query("SELECT * FROM user WHERE userName==:login")
    fun getUserByLogin(login: String): LiveData<UserEntity?>

    @Query("SELECT * FROM user WHERE email==:email")
    fun getUserByEmail(email: String): LiveData<UserEntity?>

}

