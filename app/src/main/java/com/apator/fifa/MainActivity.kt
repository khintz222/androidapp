package com.apator.fifa

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.apator.fifa.databases.entitis.UserEntity
import com.apator.fifa.databases.viewmodels.UserViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private val userViewModel: UserViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userViewModel.insertUser(UserEntity(1, "admin", "amin@fifa.pl", "admin"))
        setContentView(R.layout.activity_main)


    }

}